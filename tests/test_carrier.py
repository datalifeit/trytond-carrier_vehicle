# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
from decimal import Decimal
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool


class CarrierVehicleTestCase(ModuleTestCase):
    """Test carrier vehicle module"""
    module = 'carrier_vehicle'

    @with_transaction()
    def test_type(self):
        """Create vehicle type"""
        Vehicletype = Pool().get('carrier.vehicle.type')

        vtype, = Vehicletype.create([
            {'name': 'Lorry',
             'active': True}])
        self.assert_(vtype.id)

    @with_transaction()
    def test_vehicle(self):
        """Create vehicle type"""
        pool = Pool()
        Carrier = pool.get('carrier')
        Vehicle = pool.get('carrier.vehicle')
        Vehicletype = pool.get('carrier.vehicle.type')
        Uom = pool.get('product.uom')
        Template = pool.get('product.template')
        Product = pool.get('product.product')
        Party = pool.get('party.party')

        unit, = Uom.search([('name', '=', 'Unit')])
        template, = Template.create([{
            'name': 'Transport',
            'type': 'service',
            'list_price': Decimal(500),
            'cost_price_method': 'fixed',
            'default_uom': unit.id,
        }])
        product, = Product.create([{
            'template': template.id,
            'cost_price': Decimal(0),
        }])
        party, = Party.create([{
            'name': 'Carrier 1'
        }])
        carrier, = Carrier.create([
            {'party': party.id,
             'carrier_product': product.id,
             'carrier_cost_method': 'product'}])
        self.assert_(carrier.id)

        vtype, = Vehicletype.create([
            {'name': 'Lorry',
             'active': True}])
        vh, = Vehicle.create([{
            'carrier': carrier.id,
            'type': vtype.id,
            'number': 'MX3449',
            'trailer_number': 'MX9999',
            'driver': 'Juan R.',
            'driver_identifier': 'JR',
            'active': True
        }])
        self.assert_(vh.id)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        CarrierVehicleTestCase))
    return suite
