import datetime
from decimal import Decimal
from proteus import Model
today = datetime.date.today()


def create_carrier(config=None):
    Carrier = Model.get('carrier')
    VehicleType = Model.get('carrier.vehicle.type')
    Party = Model.get('party.party')
    Template = Model.get('product.template')
    Uom = Model.get('product.uom')

    type = VehicleType(name='Lorry')
    type.save()

    unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    template = Template(
        name='Transport',
        type='service',
        list_price=Decimal(500),
        cost_price=Decimal(0),
        default_uom=unit)
    template.save()

    party = Party(name='Carrier 1')
    party.save()

    carrier = Carrier()
    carrier.party = party
    carrier.carrier_product = template.products[0]
    vh = carrier.vehicles.new()
    vh.type = type
    vh.number = 'MX3449'
    carrier.save()
    return carrier
